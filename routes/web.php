<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('about', 'AboutController@index')->name('about');
Route::get('style', 'StyleController@index')->name('style');
Route::get('/service', 'ServiceController@index');
Route::get('recruit', 'RecruitController@index')->name('recruit');
Route::get('contact', 'ContactController@index')->name('contact');
Route::get('blog', 'BlogController@index')->name('blog');
Route::get('blog/category/{id}', 'BlogController@category');
Route::get('blog/tag/{id}', 'BlogController@tag');
Route::get('blog/{id}/detail', 'BlogController@detail');
Route::get('blog/search', 'BlogController@search');
Route::get('/event', 'EventController@index');
Route::get('/event/{id}/detail', 'EventController@detail');
Route::get('/gallery', 'GalleryController@index');
Route::get('/recruit', 'RecruitController@index');
Route::get('/sitemap', 'SitemapController@index');
Route::get('/team', 'TeamController@index');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
