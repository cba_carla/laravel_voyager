<ul class="nav nav-pills" id="mainNav">
	@foreach($items as $menu_item)
		@php
            $submenu = $menu_item->children;
        @endphp
        @if(isset($submenu) && $submenu->count())
        	<li class="dropdown">
        		<a href="{{ $menu_item->link() }}" class="dropdown-toggle">{{ $menu_item->title }}</a>
	        	<ul class="dropdown-menu">
	                @foreach($submenu as $item)
	                    <li><a href="{{$item->url}}">{{$item->title}} </a></li>
	                @endforeach
	            </ul>
            </li>
        @else
        	<li><a href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a></li>
        @endif
    @endforeach

</ul>