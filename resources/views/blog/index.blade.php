@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">@lang('strings.home')</a></li>
						@if(isset($data))
						@if(isset($data->category))<li>Category</li>@else <li>Tag</li>@endif <li class="active">{{ $data->name }}</li>
						@else
						<li class="active">@lang('strings.blogs')</li>
						@endif
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					@if(isset($data))
						<h1>@if(isset($data->category))Category: @else Tag: @endif{{ $data->name }}</h1>
					@else
						<h1>@lang('strings.blogs')</h1>
					@endif
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="blog-posts">

				@if(isset($posts) && $posts->count())
					@foreach($posts as $post)
					<article class="post post-large">
						@if($post->photo_format->count())
						<div class="post-image">
							<div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
								@foreach($post->photo_format as $photo)
								<div>
									<div class="img-thumbnail">
										<img class="img-responsive" src="{{ $photo->url }}" alt="">
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@endif

						<div class="post-date">
							<span class="day">{{ $post->created_at->format('d') }}</span>
							<span class="month">{{ $post->created_at->format('M') }}</span>
						</div>

						<div class="post-content">

							<h2><a href="/blog/{{ $post->id }}/detail">{{ $post->title }}</a></h2>
							<p>{{ $post->excerpt }} [...]</p>

							<div class="post-meta">
								<span><i class="fa fa-archive"></i> <a href="/blog/category/{{ $post->category->id }}">{{ $post->category->name }}</a> </span>
								@if(count($post->tags))
									<span><i class="fa fa-tag"></i> 
										@foreach($post->tags as $k => $tag)
											@if($k>0), @endif
											<a href="/blog/tag/{{ $tag->id }}">{{ $tag->name }}</a>
										@endforeach
									</span>
								@endif
								<a href="/blog/{{ $post->id }}/detail" class="btn btn-xs btn-primary pull-right">@lang('strings.read_more')...</a>
							</div>

						</div>
					</article>
					@endforeach

					{{ $posts->links() }}
				@endif

				</div>
			</div>

		</div>

	</div>
@endsection