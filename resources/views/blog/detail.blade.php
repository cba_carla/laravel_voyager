@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Blog</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Blog Post</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-9">
				<div class="blog-posts single-post">

					<article class="post post-large blog-single-post">
					@if(isset($post))
						@if($post->photo_format->count())
						<div class="post-image">
							<div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
								@foreach($post->photo_format as $photo)
								<div>
									<div class="img-thumbnail">
										<img class="img-responsive" src="{{ $photo->url }}" alt="">
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@endif

						<div class="post-date">
							<span class="day">{{ $post->created_at->format('d') }}</span>
							<span class="month">{{ $post->created_at->format('M') }}</span>
						</div>

						<div class="post-content">

							<h2><a href="/blog/{{ $post->id }}/detail">{{ $post->title }}</a></h2>

							<div class="post-meta">
								<span><i class="fa fa-archive"></i> <a href="/blog/category/{{ $post->category->id }}">{{ $post->category->name }}</a> </span>
								@if(count($post->tags))
									<span><i class="fa fa-tag"></i> 
										@foreach($post->tags as $k => $tag)
											@if($k>0), @endif
											<a href="/blog/tag/{{ $tag->id }}">{{ $tag->name }}</a>
										@endforeach
									</span>
								@endif
							</div>

							{!! $post->content !!}

							<div class="post-block post-share">
								<h3 class="heading-primary"><i class="fa fa-share"></i>Share this post</h3>

								<!-- AddThis Button BEGIN -->
								<div class="addthis_toolbox addthis_default_style ">
									<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
									<a class="addthis_button_tweet"></a>
									<a class="addthis_button_pinterest_pinit"></a>
									<a class="addthis_counter addthis_pill_style"></a>
								</div>
								<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
								<!-- AddThis Button END -->

							</div>

							<div class="post-block post-author clearfix">
								<h3 class="heading-primary"><i class="fa fa-user"></i>Author</h3>
								<div class="img-thumbnail">
									<a href="blog-post.html">
										<img src="/img/avatars/avatar.jpg" alt="">
									</a>
								</div>
								<p><strong class="name"><a href="#">John Doe</a></strong></p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui. </p>
							</div>

						</div>
					</article>
					@endif
				</div>
			</div>

			<div class="col-md-3">
				<aside class="sidebar">
				
					<form action="/blog/search">
						<div class="input-group input-group-lg">
							<input class="form-control" placeholder="Search..." name="s" id="s" type="text">
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary btn-lg"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
				
					<hr>
				
					<h4 class="heading-primary">Categories</h4>
					<ul class="nav nav-list mb-xlg">
					@if(isset($category))
						{!! App\Helpers\Helper::getFrontendCategory($category) !!}
					@endif
					</ul>
					<div class="tabs mb-xlg">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i> Recent</a></li>
							<li><a href="#recentPosts" data-toggle="tab">Older</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="popularPosts">
								<ul class="simple-post-list">
									@if(isset($recent_posts))
									@foreach($recent_posts as $p)
									<li>
										@if($post->photo_format->count())
										<div class="post-image">
											<div class="img-thumbnail">
												<a href="/blog/{{ $p->id }}/detail">
														<img src="{{ $post->photo_format[0]->url }}" alt="">
												</a>
											</div>
										</div>
										@endif
										<div class="post-info">
											<a href="/blog/{{ $p->id }}/detail">{{ $p->title }}</a>
											<div class="post-meta">{{ $p->created_at->format('M d, Y') }}</div>
										</div>
									</li>
									@endforeach
									@endif
								</ul>
							</div>
							<div class="tab-pane" id="recentPosts">
								<ul class="simple-post-list">
								@if(isset($oldest_posts))
									@foreach($oldest_posts as $p)
									<li>
										@if(isset($post) && $post->photo_format->count())
										<div class="post-image">
											<div class="img-thumbnail">
												<a href="/blog/{{ $p->id }}/detail">
														<img src="{{ $post->photo_format[0]->url }}" alt="">
												</a>
											</div>
										</div>
										@endif
										<div class="post-info">
											<a href="/blog/{{ $p->id }}/detail">{{ $p->title }}</a>
											<div class="post-meta">{{ $p->created_at->format('M d, Y') }}</div>
										</div>
									</li>
									@endforeach
								@endif
								</ul>
							</div>
						</div>
					</div>
				
					<hr>
				
					<h4 class="heading-primary">About Us</h4>
					<p>Nulla nunc dui, tristique in semper vel, congue sed ligula. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. Nulla nunc dui, tristique in semper vel. Nam dolor ligula, faucibus id sodales in, auctor fringilla libero. </p>
				
				</aside>
			</div>
		</div>

	</div>
@endsection
