@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Search</li>
						@if($keyword)<li class="active">{{ $keyword }}</li>@endif
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Search @if($keyword)Results for: {{ $keyword }}@endif</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-9">
				<h5 class="mb-none">New search:</h5>
				<p>If you are not happy with the results below please do another search:</p>

				<form>
					<div class="input-group">
						<input class="form-control" placeholder="Search..." name="s" id="s" type="text" >
						<span class="input-group-btn">
							<button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
						</span>
					</div>
				</form>

				<hr>

				@if($posts && $posts->count())
				<h4 class="heading-primary">{{ $posts->total() }} search results for: <strong>{{ $keyword }}</strong></h4>

				<ul class="simple-post-list">
					@foreach($posts as $post)
					<li>
						<div class="post-info">
							<a href="/blog/{{ $post->id }}/detail">{{ $post->title }}</a>
							<div class="post-meta">{{ $post->created_at->format('M d, Y') }}</div>
						</div>
					</li>
					@endforeach

				</ul>
					{{ $posts->links() }}
				@endif

			</div>

		</div>

	</div>
@endsection
