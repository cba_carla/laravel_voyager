@extends('layouts.app')

@section('htmlheader_title', 'Văn Hóa CBA')
@section('pageId','pageStyle')
@section('main-content')

<div class="keyv-style">
	<div class="container">
		<div class="row-fluid">
			<div class="span12">
				<div class="info-title-about">
					<h2 class="headline2">VĂN HÓA CBA</h2>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block-section02 content-style">
	<div class="container">
		<ul class="clearfix">
			<li class="item clearfix">
				<div class="style-img">
					<img src="/images/style/img_style01.jpg" alt="">
				</div>
				<div class="style-text">
					<h2>Ngày hội thể thao ngoài trời</h2>
					<p class="text-info">Ngày hội thể thao ngoài trời của CYBRIDGE ASIA, khởi động với bài tập thể dục buổi sáng. Với các môn thi đấu đồng đội vô cùng kịch tính: chạy tiếp sức, bước đều đồng đội, giật cờ, nhảy dây chạy giữ thăng bằng, …, mỗi thành viên là một…</p>
					<p class="box-btn"><a class="btn-style" href="#">Đọc thêm</a></p>
				</div>
			</li>

			<li class="item clearfix">
				<div class="style-img">
					<img src="/images/style/img_style02.jpg" alt="">
				</div>
				<div class="style-text">
					<h2>Ngày hội thể thao ngoài trời</h2>
					<p class="text-info">Ngày hội thể thao ngoài trời của CYBRIDGE ASIA, khởi động với bài tập thể dục buổi sáng. Với các môn thi đấu đồng đội vô cùng kịch tính: chạy tiếp sức, bước đều đồng đội, giật cờ, nhảy dây chạy giữ thăng bằng, …, mỗi thành viên là một…</p>
					<p class="box-btn"><a class="btn-style" href="#">Đọc thêm</a></p>
				</div>
			</li>

			<li class="item clearfix">
				<div class="style-img">
					<img src="/images/style/img_style03.jpg" alt="">
				</div>
				<div class="style-text">
					<h2>Ngày hội thể thao ngoài trời</h2>
					<p class="text-info">Ngày hội thể thao ngoài trời của CYBRIDGE ASIA, khởi động với bài tập thể dục buổi sáng. Với các môn thi đấu đồng đội vô cùng kịch tính: chạy tiếp sức, bước đều đồng đội, giật cờ, nhảy dây chạy giữ thăng bằng, …, mỗi thành viên là một…</p>
					<p class="box-btn"><a class="btn-style" href="#">Đọc thêm</a></p>
				</div>
			</li>

			<li class="item clearfix">
				<div class="style-img">
					<img src="/images/style/img_style04.jpg" alt="">
				</div>
				<div class="style-text">
					<h2>Ngày hội thể thao ngoài trời</h2>
					<p class="text-info">Ngày hội thể thao ngoài trời của CYBRIDGE ASIA, khởi động với bài tập thể dục buổi sáng. Với các môn thi đấu đồng đội vô cùng kịch tính: chạy tiếp sức, bước đều đồng đội, giật cờ, nhảy dây chạy giữ thăng bằng, …, mỗi thành viên là một…</p>
					<p class="box-btn"><a class="btn-style" href="#">Đọc thêm</a></p>
				</div>
			</li>
		</ul>
		<div class="pagination-container">
			<ul class="pagination">
				<li>
					<a href="#">
					<span>←</span>
					</a>
				</li>
				<li class="active">
					<a href="#">1</a>
				</li>
				<li>
					<a href="#">2</a>
				</li>
				<li>
					<a href="#">3</a>
				</li>
				<li>
					<a href="#">
						<span>→</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>

<div id="full-division-box">
	<div class="full-bg-image full-bg-image-fixed full-bg-image-fixed02" style="background-position: center 192px;"></div>
	<div class="container full-content-box">
		<div class="row-fluid">
			<div class="span12">
				<div class="box-client box-cba-music">
					<div class="element-wrap">
						<h3 class="cba-music">CBA SONG<br>
						<span>© Lyrics: Travis &amp; Thom / Composer: Thom
With all staff of CYBRiDGE ASIA</span></h3>
						<p class="actions"><a class="btn-song" href="https://cybridgeasia.vn/en/song/">VIEW VIDEO</a></p>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>



@endsection
@push('styles')
<link rel="stylesheet" href="/css/culture.css" media="all">
@endpush
