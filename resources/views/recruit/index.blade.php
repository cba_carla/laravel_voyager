<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Careers | Porto - Responsive HTML5 Template 5.5.0</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="/img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/css/theme.css">
		<link rel="stylesheet" href="/css/theme-elements.css">
		<link rel="stylesheet" href="/css/theme-blog.css">
		<link rel="stylesheet" href="/css/theme-shop.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/css/custom.css">

		<!-- Head Libs -->
		<script src="/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>

		<div class="body">
			@include('includes.header')

			<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="/">Home</a></li>
									<li class="active">Pages</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h1>Careers</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<h2><strong>Rockstars</strong> wanted!</h2>

					<div class="row">
						<div class="col-md-12">
							<p class="lead">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rutrum pellentesque imperdiet. Nulla lacinia iaculis nulla non pulvinar. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Ut eu risus enim, ut pulvinar lectus. Sed hendrerit nibh metus.
							</p>
						</div>
					</div>

					<hr>

					<div class="row">
						<div class="col-md-8">
							<div class="toggle toggle-primary mt-lg" data-plugin-toggle>
							@if(isset($datas))
								@foreach($datas as $data)
								<section class="toggle">
									<label>{{ $data->title }}</label>
									<div class="toggle-content">
										<p><strong>Location:</strong> {{ $data->location }} - <strong>Department:</strong> {{ $data->position }}</p>
										<p>{{ $data->description }}</p>
										<p><a class="btn btn-primary mb-xl" href="{{ $url }}/{{ $data->id }}/detail">Apply Now</a></p>
									</div>
								</section>
								@endforeach
							@endif
							</div>
						</div>
						<div class="col-md-4">
							<div class="featured-box featured-box-primary">
								<div class="box-content">
									<h4 class="text-uppercase">The Benefits</h4>
									<ul class="thumbnail-gallery" data-plugin-lightbox data-plugin-options="{'delegate': 'a', 'type': 'image', 'gallery': {'enabled': true}}">
										<li>
											<a title="Benefits 1" href="img/benefits/benefits-1.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-1-thumb.jpg" alt="">
												</span>
											</a>
										</li>
										<li>
											<a title="Benefits 2" href="img/benefits/benefits-2.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-2-thumb.jpg" alt="">
												</span>
											</a>
										</li>
										<li>
											<a title="Benefits 3" href="img/benefits/benefits-3.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-3-thumb.jpg" alt="">
												</span>
											</a>
										</li>
										<li>
											<a title="Benefits 4" href="img/benefits/benefits-4.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-4-thumb.jpg" alt="">
												</span>
											</a>
										</li>
										<li>
											<a title="Benefits 5" href="img/benefits/benefits-5.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-5-thumb.jpg" alt="">
												</span>
											</a>
										</li>
										<li>
											<a title="Benefits 6" href="img/benefits/benefits-6.jpg">
												<span class="thumbnail mb-none">
													<img src="img/benefits/benefits-6-thumb.jpg" alt="">
												</span>
											</a>
										</li>
									</ul>

									<hr>

									<ul class="list list-icons align-left">
										<li><i class="fa fa-check"></i>Health insurance</li>
										<li><i class="fa fa-check"></i>Retirement investment plans</li>
										<li><i class="fa fa-check"></i>Vacation time</li>
									</ul>

								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

			@include('includes.footer')
		</div>

		<!-- Vendor -->
		<script src="/vendor/jquery/jquery.min.js"></script>
		<script src="/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="/vendor/common/common.min.js"></script>
		<script src="/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="/vendor/vide/vide.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="/js/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="/js/theme.init.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->

	</body>
</html>
