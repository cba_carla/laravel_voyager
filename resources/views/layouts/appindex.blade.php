<!doctype html>
<html lang="ja">

@section('htmlheader')
	@include('layouts.partials.htmlheader')
@show
<body id="pageHome">
<div id="wrapper">

	@include('layouts.partials.mainheader')

	<!-- Content Wrapper. Contains page content -->
	<div id="main" class="clearfix">
		<!-- Main content -->
		@include('layouts.partials.mainslider')
		@yield('main-content')
	</div><!-- /.content-wrapper -->

	@include('layouts.partials.footer')

</div><!-- ./wrapper -->

@section('scripts')
	@include('layouts.partials.scripts')
@show

</body>
</html>
