@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Shortcodes</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Image Gallery</h1>
				</div>
			</div>
		</div>
	</section>
@if(isset($albums) && $albums->count())
@foreach($albums as $album)
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">
				<h4>{{ $album->name }}</h4>
			</div>
		</div>
	</div>

	<ul class="image-gallery sort-destination full-width">
		@foreach($album->photo as $k => $v)
		<li class="isotope-item">
			<div class="image-gallery-item">
				<a href="javascript:;">
					<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
						<span class="thumb-info-wrapper">
							<img src="{{ $v->url }}" class="img-responsive" alt="">
							<span class="thumb-info-action">
								<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
							</span>
						</span>
					</span>
				</a>
			</div>
		</li>
		@endforeach
	</ul>
@endforeach
@endif
	{{-- <div class="container">

		<div class="row">
			<div class="col-md-12">
				<hr class="tall">

				<h4>{{ $album->name }}</h4>

				<div class="masonry-loader masonry-loader-showing">
					<div class="masonry" data-plugin-masonry data-plugin-options="{'itemSelector': '.masonry-item'}">
						@foreach($album->photo as $k => $v)
						<div class="masonry-item">
							<a href="javascript:;">
								<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
									<span class="thumb-info-wrapper">
										<img src="{{ $v->url }}" class="img-responsive" alt="">
										<span class="thumb-info-action">
											<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
										</span>
									</span>
								</span>
							</a>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>

	</div> --}}
@endsection

@section('javascript')
	<!-- Examples -->
	<script src="/js/examples/examples.gallery.js"></script>
@stop
