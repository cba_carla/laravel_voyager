@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Pages</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Team</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<h2>Meet the <strong>Team</strong></h2>

		<ul class="nav nav-pills sort-source" data-sort-id="team" data-option-key="filter">
			<li data-option-value="*" class="active"><a href="javascript:;">Show All</a></li>
			@if(isset($teams))
			@foreach($teams as $team)
				<li data-option-value=".team-{{ $team->id }}"><a href="javascript:;">{{ $team->name }}</a></li>
			@endforeach
			@endif
		</ul>

		<hr>

		<div class="row">

			<div class="sort-destination-loader sort-destination-loader-showing">
				<ul class="team-list sort-destination" data-sort-id="team">
					@if(isset($members))
					@foreach($members as $member)
					<li class="col-md-3 col-sm-6 col-xs-12 isotope-item team-{{ $member->team->id }}">
						<span class="thumb-info thumb-info-hide-wrapper-bg mb-xlg">
							<span class="thumb-info-wrapper">
								<a href="javascript:;">
									@if($member->photo_format->count())
										@foreach($member->photo_format as $k => $photo)
											@if($k==0)
											<img class="img-responsive" src="{{ $photo->url }}" alt="">
											@endif
										@endforeach
									@endif
									<span class="thumb-info-title">
										<span class="thumb-info-inner">{{ $member->name }}</span>
										<span class="thumb-info-type">{{ $member->position }}</span>
									</span>
								</a>
							</span>
							<span class="thumb-info-caption">
								<span class="thumb-info-caption-text">{!! $member->description !!}</span>
								@if(!empty($member->social) && $member->social_array)
								    <span class="thumb-info-social-icons">
								  @foreach($member->social_array as $sa)
								    @foreach($sa as $k => $s)

								    	<a target="_blank" href="{{ $s }}"><i class="fa fa-{{ $k }}"></i></a>

								    @endforeach
								  @endforeach
								    </span>
								@endif
								
							</span>
						</span>
					</li>
					@endforeach
					@endif
				</ul>
			</div>

		</div>

	</div>
@endsection