@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Contact Us</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Contact Us</h1>
				</div>
			</div>
		</div>
	</section>

	<!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
	<div id="googlemaps" class="google-map"></div>

	<div class="container">

		<div class="row">
			<div class="col-md-6">
				
				@if(Session::has('messages'))
				<div class="alert alert-success mt-lg" id="contactSuccess">
					{!! Session::get('messages') !!}
				</div>
				@endif
				@if($errors->any())
				<div class="alert alert-danger mt-lg" id="contactError">
					<strong>Error!</strong> There was an error sending your message.<br>
					@foreach($errors->all() as $error)
							<strong>{{ $error }}</strong><br>
					@endforeach
					<span class="font-size-xs mt-sm display-block" id="mailErrorMessage"></span>
				</div>
				@endif

				<h2 class="mb-sm mt-sm"><strong>Contact</strong> Us</h2>
						
				
				{!! Form::open(['url'=>'/contact']) !!}
					
					<div class="row">
						<div class="form-group">
							<div class="col-md-6">
								<label>Your name *</label>
								<input type="text" value="{{ old('name') }}" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
								@if ($errors->has('name'))<span class="help-block">{{ $errors->first('name') }}</span>@endif
							</div>
							<div class="col-md-6">
								<label>Your email address *</label>
								<input type="email" value="{{ old('email') }}" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
								@if ($errors->has('email'))<span class="help-block">{{ $errors->first('email') }}</span>@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-6">
								<label>Your phone *</label>
								<input type="text" value="{{ old('phone') }}" data-msg-required="Please enter your phone." maxlength="100" class="form-control" name="phone" id="phone" required>
								@if ($errors->has('phone'))<span class="help-block">{{ $errors->first('phone') }}</span>@endif
							</div>
							<div class="col-md-6">
								<label>Your address *</label>
								<input type="text" value="{{ old('address') }}" data-msg-required="Please enter your address." maxlength="100" class="form-control" name="address" id="address" required>
								@if ($errors->has('address'))<span class="help-block">{{ $errors->first('address') }}</span>@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<label>Subject</label>
								<input type="text" value="{{ old('title') }}" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="title" id="subject" required>
								@if ($errors->has('title'))<span class="help-block">{{ $errors->first('title') }}</span>@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
								<label>Message *</label>
								<textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="content" id="message" required>{{ old('content') }}</textarea>
								@if ($errors->has('content'))<span class="help-block">{{ $errors->first('content') }}</span>@endif
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="Send Message" class="btn btn-primary btn-lg mb-xlg" data-loading-text="Loading...">
						</div>
					</div>
				{!! Form::close() !!}
			</div>
			<div class="col-md-6">

				<h4 class="heading-primary mt-lg">Get in <strong>Touch</strong></h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit imperdiet varius. In eu ipsum vitae velit congue iaculis vitae at risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

				<hr>

				<h4 class="heading-primary">The <strong>Office</strong></h4>
				<ul class="list list-icons list-icons-style-3 mt-xlg">
					<li><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</li>
					<li><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-789</li>
					<li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></li>
				</ul>

				<hr>

				<h4 class="heading-primary">Business <strong>Hours</strong></h4>
				<ul class="list list-icons list-dark mt-xlg">
					<li><i class="fa fa-clock-o"></i> Monday - Friday - 9am to 5pm</li>
					<li><i class="fa fa-clock-o"></i> Saturday - 9am to 2pm</li>
					<li><i class="fa fa-clock-o"></i> Sunday - Closed</li>
				</ul>

			</div>

		</div>

	</div>
@endsection
@section('addhtml')
	<section class="call-to-action call-to-action-default with-button-arrow call-to-action-in-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="call-to-action-content">
						<h3>Porto is <strong>everything</strong> you need to create an <strong>awesome</strong> website!</h3>
						<p>The <strong>#1 Selling</strong> HTML Site Template on ThemeForest</p>
					</div>
					<div class="call-to-action-btn">
						<a href="http://themeforest.net/item/porto-responsive-html5-template/4106987" target="_blank" class="btn btn-lg btn-primary">Buy Now!</a><span class="arrow hlb hidden-xs hidden-sm hidden-md" data-appear-animation="rotateInUpLeft" style="top: -12px;"></span>
					</div>
				</div>
			</div>
		</div>
	</section>
@stop
@section('javascript')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC-CQdWN5u3kMi04OJEbDgDnxZFh7qlEG8"></script>
		<script>
			/*
			Map Settings

				Find the Latitude and Longitude of your address:
					- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
					- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

			*/
			// Map Markers
			var mapMarkers = [{
				address: "New York, NY 10017",
				html: "<strong>New York Office</strong><br>New York, NY 10017",
				icon: {
					image: "/img/pin.png",
					iconsize: [26, 46],
					iconanchor: [12, 46]
				},
				popup: true
			}];

			// Map Initial Location
			var initLatitude = 10.7513756;
			var initLongitude = 106.6939804;

			// Map Extended Settings
			var mapSettings = {
				controls: {
					panControl: true,
					zoomControl: true,
					mapTypeControl: true,
					scaleControl: true,
					streetViewControl: true,
					overviewMapControl: true
				},
				scrollwheel: false,
				markers: mapMarkers,
				latitude: initLatitude,
				longitude: initLongitude,
				zoom: 16
			};

			var map = $('#googlemaps').gMap(mapSettings);

			// Map Center At
			var mapCenterAt = function(options, e) {
				e.preventDefault();
				$('#googlemaps').gMap("centerAt", options);
			}

		</script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->
@stop
