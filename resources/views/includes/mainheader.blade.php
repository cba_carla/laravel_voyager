<div id="header" class="skehead-headernav clearfix piecss3">
	<div class="header-top-sp clearfix">
		<div class="container">
			<ul class="list-inline spDisplay clearfix">
				<li class="language clearfix">
					<p class="open">
						<a href="#vietnam"><img src="/images/common/blank.gif" class="flag flag-vn" alt="vietnam">Vietnamese<i class="fa fa-sort-down"></i>
						</a>
					</p>
					<ul class="inner-language">
						<li>
							<a href="#vietnam"><img src="/images/common/blank.gif" class="flag flag-vn" alt="vietnam">Vietnamese</a>
						</li>
						<li>
							<a href="#english"><img src="/images/common/blank.gif" class="flag flag-us" alt="english">English</a>
						</li>
						<li>
							<a href="#japanese"><img src="/images/common/blank.gif" class="flag flag-jp" alt="japanese">Japanese</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<div class="header-top-pc clearfix">
		<div class="container">
			<div class="box-logo"><a href="index.html" title="" id="logo"> <img src="/images/common/logo.png" alt=""> </a> </div>
			<ul class="list-inline pcDisplay clearfix">
				<li class="language clearfix">
					<p class="open">
						<a href="#vietnam"><img src="/images/common/blank.gif" class="flag flag-vn" alt="vietnam">Vietnamese<i class="fa fa-sort-down"></i>
						</a>
					</p>
					<ul class="inner-language">
						<li>
							<a href="#vietnam"><img src="/images/common/blank.gif" class="flag flag-vn" alt="vietnam">Vietnamese</a>
						</li>
						<li>
							<a href="#english"><img src="/images/common/blank.gif" class="flag flag-us" alt="english">English</a>
						</li>
						<li>
							<a href="#japanese"><img src="/images/common/blank.gif" class="flag flag-jp" alt="japanese">Japanese</a>
						</li>
					</ul>
				</li>
			</ul>
			<div class="block-nav pcDisplay">
				<ul class="menu clearfix">
					<li class="active item"><a href="/"><span>TRANG CHỦ</span></a>
					</li>
					<li class="item"><a href="/about"><span>GIỚI THIỆU</span></a>
					</li>
					<li class="item"><a href="/style"><span>VĂN HÓA CBA</span></a>
					</li>
					<li class="item"><a href="/services"><span>DỊCH VỤ</span></a>
					</li>
					<li class="item"><a href="/recruit"><span>TUYỂN DỤNG</span></a>
					</li>
					<li class="item"><a href="/contact"><span>LIÊN HỆ</span></a>
					</li>
				</ul>
			</div>
		</div>
		<p class="openNav spDisplay"><a class="menu-trigger" href="javascript:void(0);"><span>MENU</span></a></p>
	</div>
	<div class="menuSp spDisplay">
		<nav id="navi" class="spDisplay">
			<ul class="clearfix">
				<li><a href="/"><span>TRANG CHỦ</span></a></li>
				<li><a href="/about"><span>GIỚI THIỆU</span></a></li>
				<li><a href="/style"><span>VĂN HÓA CBA</span></a></li>
				<li><a href="/services"><span>DỊCH VỤ</span></a></li>
				<li><a href="/recruit"><span>TUYỂN DỤNG</span></a></li>
				<li><a href="/contact"><span>LIÊN HỆ</span></a></li>
			</ul>
		</nav>
		<!-- / #nav -->
	</div>
</div>
