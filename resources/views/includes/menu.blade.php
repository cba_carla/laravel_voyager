@if(Module::find('blog')->isStatus(1))
<li class="treeview">
  <a href="#">
    <i class="fa fa-pencil-square"></i> <span>@lang('strings.blogs')</span>
    <span class="pull-right-container">
      <small class="fa fa-angle-left pull-right"></small>
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.categories')">{{ App\Helpers\Helper::CountDb('post_categories') }}</small>
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.posts')">{{ App\Helpers\Helper::CountDb('posts') }}</small>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="/admin/blog/post"><i class="fa fa-circle-o"></i> @lang('strings.all_post')
    <span class="pull-right-container">
      <small class="label pull-right bg-green">{{ App\Helpers\Helper::CountDb('posts') }}</small>
    </span></a></li>
    
    <li><a href="/admin/blog/post/create"><i class="fa fa-pencil-square-o text-red"></i> @lang('strings.create_new_post')</a></li>
    
    <li><a href="/admin/blog/category"><i class="fa fa-archive text-yellow"></i> @lang('strings.all_categories')
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow">{{ App\Helpers\Helper::CountDb('post_categories') }}</small>
    </span></a></li>

    <li><a href="/admin/blog/tag"><i class="fa fa-tags text-blue"></i> @lang('strings.all_tags')
    <span class="pull-right-container">
      <small class="label pull-right bg-blue">{{ App\Helpers\Helper::CountDb('post_tags') }}</small>
    </span></a></li>
  </ul>
</li>
@endif
@if(Module::find('client')->isStatus(1))
<li>
  <a href="/admin/client">
    <i class="fa fa-trademark"></i> <span>@lang('strings.clients')</span>
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.clients')">{{ App\Helpers\Helper::CountDb('clients') }}</small>
    </span>
  </a>
</li>
@endif
@if(Module::find('contact')->isStatus(1))
<li>
  <a href="/admin/contact">
    <i class="fa fa-credit-card"></i> <span>@lang('strings.contacts')</span>
    <span class="pull-right-container">
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.contacts')">{{ App\Helpers\Helper::CountDb('contacts') }}</small>
    </span>
  </a>
</li>
@endif
@if(Module::find('event')->isStatus(1))
<li>
  <a href="/admin/event">
    <i class="fa fa-bullhorn"></i> <span>@lang('strings.events')</span>
    <span class="pull-right-container">
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.events')">{{ App\Helpers\Helper::CountDb('events') }}</small>
    </span>
  </a>
</li>
@endif
@if(Module::find('gallery')->isStatus(1))
<li>
  <a href="/admin/gallery">
    <i class="fa fa-photo"></i> <span>@lang('strings.galleries')</span>
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.albums')">{{ App\Helpers\Helper::CountDb('albums') }}</small>
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.photos')">{{ App\Helpers\Helper::CountDb('photos') }}</small>
    </span>
  </a>
</li>
@endif
@if(Module::find('product')->isStatus(1))
<li class="treeview">
  <a href="#">
    <i class="fa fa-shopping-basket"></i> <span>@lang('strings.products')</span>
    <span class="pull-right-container">
      <small class="fa fa-angle-left pull-right"></small>
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.catalogs')">{{ App\Helpers\Helper::CountDb('product_catalogs') }}</small>
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.products')">{{ App\Helpers\Helper::CountDb('products') }}</small>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="/admin/product"><i class="fa fa-circle-o"></i> @lang('strings.all_product')
    <span class="pull-right-container">
      <small class="label pull-right bg-green">{{ App\Helpers\Helper::CountDb('products') }}</small>
    </span></a></li>
    
    <li><a href="/admin/product/create"><i class="fa fa-circle-o text-red"></i> @lang('strings.create_new_product')</a></li>
    
    <li><a href="/admin/product/catalog"><i class="fa fa-archive text-yellow"></i> @lang('strings.all_catalogs')
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow">{{ App\Helpers\Helper::CountDb('product_catalogs') }}</small>
    </span></a></li>

    <li><a href="/admin/product/tag"><i class="fa fa-tags text-blue"></i> @lang('strings.all_tags')
    <span class="pull-right-container">
      <small class="label pull-right bg-blue">{{ App\Helpers\Helper::CountDb('product_tags') }}</small>
    </span></a></li>

    <li><a href="/admin/product/attribute"><i class="fa fa-sitemap text-aqua"></i> @lang('strings.all_attributes')
    <span class="pull-right-container">
      <small class="label pull-right bg-aqua">{{ App\Helpers\Helper::CountDb('product_attributes') }}</small>
    </span></a></li>

  </ul>
</li>
@endif
@if(Module::find('recruit')->isStatus(1))
<li class="treeview">
  <a href="#">
    <i class="fa fa-book"></i> <span>@lang('strings.recruits')</span>
    <span class="pull-right-container">
      <small class="fa fa-angle-left pull-right"></small>
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.applies')">{{ App\Helpers\Helper::CountDb('applies') }}</small>
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.recruit')">{{ App\Helpers\Helper::CountDb('recruits') }}</small>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="/admin/recruit"><i class="fa fa-coffee"></i> @lang('strings.all_recruits')
    <span class="pull-right-container">
      <small class="label pull-right bg-green">{{ App\Helpers\Helper::CountDb('recruits') }}</small>
    </span></a></li>
    
    <li><a href="/admin/applies"><i class="fa fa-ticket text-yellow"></i> @lang('strings.all_applies')
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow">{{ App\Helpers\Helper::CountDb('applies') }}</small>
    </span></a></li>

  </ul>
</li>
@endif
@if(Module::find('team')->isStatus(1))
<li class="treeview">
  <a href="#">
    <i class="fa fa-users"></i> <span>@lang('strings.teams')</span>
    <span class="pull-right-container">
      <small class="fa fa-angle-left pull-right"></small>
      <small class="label pull-right bg-yellow" data-toggle="tooltip" data-original-title="@lang('strings.teams')">{{ App\Helpers\Helper::CountDb('teams') }}</small>
      <small class="label pull-right bg-green" data-toggle="tooltip" data-original-title="@lang('strings.members')">{{ App\Helpers\Helper::CountDb('members') }}</small>
    </span>
  </a>
  <ul class="treeview-menu">
    <li><a href="/admin/team"><i class="fa fa-users text-yellow"></i> @lang('strings.all_team')
    <span class="pull-right-container">
      <small class="label pull-right bg-yellow">{{ App\Helpers\Helper::CountDb('teams') }}</small>
    </span></a></li>

    <li><a href="/admin/member"><i class="fa fa-user-secret"></i> @lang('strings.all_member')
    <span class="pull-right-container">
      <small class="label pull-right bg-green">{{ App\Helpers\Helper::CountDb('members') }}</small>
    </span></a></li>

  </ul>
</li>
@endif
<li class="header">MANAGERMENT</li>
<li>
  <a href="/admin/1/information">
    <i class="fa fa-briefcase"></i> <span>Admin option</span>
  </a>
</li>