<head>
<!-- Basic -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">   

<title>{{isset($meta["title"])?$meta["title"]:""}}</title>

<meta name="keywords" content="{{isset($meta['keywords'])?rtrim($meta['keywords']):''}}">
<meta name="description" content="{{isset($meta['description'])?rtrim($meta['description']):''}}">
<meta name="author" content="{{isset($meta['author'])?rtrim($meta['author']):''}}">

<!-- facebook -->
<meta property="og:image" content="{{ asset('images/home/bg_parallax.jpg')}}">
<meta property="fb:admins" content="863536147127505">
<meta property="fb:app_id" content="863536147127505">
<meta property="og:url" content="{{Request::url()}}">
<meta property="og:title" content="{{isset($meta["title"])?$meta["title"]:""}}">
<meta property="og:site_name" content="CYBRiDGE ASIA">
<meta property="og:description" content="{{isset($meta['description'])?rtrim($meta['description']):''}}">
<meta property="og:type" content="website">
<meta property="og:locale" content="vn">
<!-- facebook -->

<!-- Favicon -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

<!-- Mobile Metas -->
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

<!-- Web Fonts  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

<!-- Vendor CSS -->
<link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/animate/animate.min.css">
<link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

<!-- Theme CSS -->
<link rel="stylesheet" href="css/theme.css">
<link rel="stylesheet" href="css/theme-elements.css">
<link rel="stylesheet" href="css/theme-blog.css">
<link rel="stylesheet" href="css/theme-shop.css">

<!-- Current Page CSS -->
<link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
<link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
<link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">

<!-- Skin CSS -->
<link rel="stylesheet" href="css/skins/default.css">

<!-- Theme Custom CSS -->
<link rel="stylesheet" href="css/custom.css">

<!-- Head Libs -->
<script src="vendor/modernizr/modernizr.min.js"></script>
<!--[if lt IE 9]>
<script src="/js/html5shiv-printshiv.js"></script>
<![endif]-->
</head>