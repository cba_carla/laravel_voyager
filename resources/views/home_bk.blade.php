@extends('layouts.appindex')
@section('htmlheader_title', "TRANG CHỦ")
@section('main-content')
<div  class="block-section bg-white">
    <div class="container">
        <div class="info-title">
            <h2 class="headline">WHY CHOOSE US?</h2>
            <div class="style-border"></div>
        </div>
        <div class="wrap-whychooseus">
            <ul class="box-whychooseus clearfix">
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus01.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus02.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus03.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="box-whychooseus box-whychooseus02 clearfix">
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus04.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus05.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
                <li class="item">
                    <div class="feature-box">
                        <div class="feature-box-icon">
                            <img src="/images/home/icon_whychooseus06.jpg" alt="">
                        </div>
                        <div class="feature-box-info">
                            <h4 class="title">Award Winning Support</h4>
                            <p class="text">Lorem ipsum dolor sit amet, consectetur adipiscing metus elit. Quisque rutrum pellentesque imperdiet.</p>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- #block-whychoseus -->

<div class="background-graph"></div>
<div  class="block-section bg-silver">
    <div class="container">
        <div class="info-title">
            <h2 class="headline">DỊCH VỤ</h2>
            <div class="style-border"></div>
        </div>
    </div>
    <div class="info-image-our-services">
        <div class="container">
        <ul class="box-image clearfix">
            <li class="view view-first item01">
                <img src="/images/common/img_services01.jpg" alt="">
                <div class="mask">
                    <p class="text01">Xây dựng trang web trên mọi thiết bị</p>
                </div>
            </li>
            <li class="view view-first item02">
                <a href=""><img src="/images/common/img_services02.jpg" alt="">
                </a>
                <div class="mask">
                    <p class="text01">Thiết kế web responsive</p>
                </div>
            </li>
            <li class="view view-first item03">
                <a href=""><img src="/images/common/img_services03.jpg" alt="">
                </a>
                <div class="mask">
                    <p class="text01">Xây dựng web trên PC</p>
                </div>
            </li>
            <li class="view view-first item04">
                <a href=""><img src="/images/common/img_services04.jpg" alt="">
                </a>
                <div class="mask">
                    <p class="text01">Dịch vụ vận hành và cập nhật bảo trì trang web</p>
                </div>
            </li>
            <li class="view view-first item05">
                <a href=""><img src="/images/common/img_services06.jpg" alt="">
                </a>
                <div class="mask">
                    <p class="text01">Dịch vụ tối ưu hóa bộ máy tìm kiếm (SEO)</p>
                </div>
            </li>
        </ul>
        </div>
    </div>
</div>
<!-- #block-our-services -->



<div id="full-division-box">
    <div class="full-bg-image  full-bg-image-fixed" style="background-position: center;"></div>
    <div class="full-content-box">
        <div class="box-whoweare clearfix">
            <div class="left-text clearfix">
                <h3 class="headline right">WHO WE ARE</h3>
                <div class="line-hover"></div>
                <p class="text-info">There are many variations of passages of Lorem Ipsum available, but the use a passage of still in their infancy Lorem Ipsum. <br><br>
Embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first combined with a handful of model sentence structures.</p>
            </div>
        </div>
    </div>
</div>
<!-- #full-division-box -->


<div  class="block-section bg-silver">
    <div class="container">
        <div class="info-title">
            <h2 class="headline">THE TEAM</h2>
            <div class="style-border"></div>
            <p class="headline-info">CYBRiDGE Á CHÂU là môi trường mà mỗi thành viên đều có tinh thần trách nhiệm trong công việc.</p>
        </div>
        <div class="info-content">
            <ul class="box-team clearfix">
                <li>
                    <img src="/images/home/img_team01.jpg" alt="">
                    <div class="textbox">
                        <h5 class="text-name">NELSON</h5>
                        <p class="text-info">Designer</p>
                    </div>
                </li>
                <li>
                    <img src="/images/home/img_team02.jpg" alt="">
                    <div class="textbox">
                        <h5 class="text-name">NELSON</h5>
                        <p class="text-info">Designer</p>
                    </div>
                </li>
                <li>
                    <img src="/images/home/img_team01.jpg" alt="">
                    <div class="textbox">
                        <h5 class="text-name">NELSON</h5>
                        <p class="text-info">Designer</p>
                    </div>
                </li>
                <li>
                    <img src="/images/home/img_team02.jpg" alt="">
                    <div class="textbox">
                        <h5 class="text-name">NELSON</h5>
                        <p class="text-info">Designer</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- block-the-team -->

<div class="background-graph02"></div>
<div  class="block-section bg-white">
    <div class="container">
        <div class="info-title">
            <h2 class="headline">BLOG CỦA CHÚNG TÔI</h2>
            <div class="style-border"></div>
        </div>
        <div class="info-content info-content02">
            <ul id="links02" class="box-team clearfix">
                <li>
                    <img src="/images/home/img_blog04.jpg" alt="">
                    <div class="distext">
                        <h5 class="headline6">LOREM IPSUM AS THEIR DEFAULT MODEL</h5>
                        <p class="info">
                            <i class="fa fa-tags" aria-hidden="true"></i>
                            &nbsp; Web design &nbsp;&nbsp;
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            &nbsp; 18/11/2016
                        </p>
                        <div class="hidtext">
                            <div class="readmore"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="/images/home/img_blog05.jpg" alt="">
                    <div class="distext">
                        <h5 class="headline6">LOREM IPSUM AS THEIR DEFAULT MODEL</h5>
                        <p class="info">
                            <i class="fa fa-tags" aria-hidden="true"></i>
                            &nbsp; Web design &nbsp;&nbsp;
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            &nbsp; 18/11/2016
                        </p>
                        <div class="hidtext">
                            <div class="readmore"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </li>
                <li>
                    <img src="/images/home/img_blog06.jpg" alt="">
                    <div class="distext">
                        <h5 class="headline6">LOREM IPSUM AS THEIR DEFAULT MODEL</h5>
                        <p class="info">
                            <i class="fa fa-tags" aria-hidden="true"></i>
                            &nbsp; Web design &nbsp;&nbsp;
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            &nbsp; 18/11/2016
                        </p>
                        <div class="hidtext">
                            <div class="readmore"><a href="#">READ MORE</a></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- block-our-blog -->


<div id="block-our-clients">
    <div class="container">
        <div class="owl-carousel owl-theme">
            <div class="item">
                <div class="item-clients">
                    <div class="img-avatar">
                        <img src="/images/home/img_member04.png" alt="avatar">
                    </div>
                    <h4 class="headline5">- NGUYỄN THỊ LỆ</h4>
                    <div class="testimonial-text">

                        <p class="text-info">“Các buổi training và seminar của công ty là điều mình thích nhất ở CBA. Mình học được kiến thức sát với thực tế và hơn thế nữa, khi làm việc tại CBA đam mê trong mình được khơi gợi và được ‘tiếp lửa’ để duy trì sự cống hiến liên tục trong công việc.”</p>

                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-clients">
                    <div class="img-avatar">
                        <img src="/images/home/img_member04.png" alt="avatar">
                    </div>
                    <h4 class="headline5">- NGUYỄN THỊ LỆ</h4>
                    <div class="testimonial-text">

                        <p class="text-info">“Các buổi training và seminar của công ty là điều mình thích nhất ở CBA. Mình học được kiến thức sát với thực tế và hơn thế nữa, khi làm việc tại CBA đam mê trong mình được khơi gợi và được ‘tiếp lửa’ để duy trì sự cống hiến liên tục trong công việc.”</p>

                    </div>
                </div>
            </div>
            <div class="item">
                <div class="item-clients">
                    <div class="img-avatar">
                        <img src="/images/home/img_member04.png" alt="avatar">
                    </div>
                    <h4 class="headline5">- NGUYỄN THỊ LỆ</h4>
                    <div class="testimonial-text">

                        <p class="text-info">“Các buổi training và seminar của công ty là điều mình thích nhất ở CBA. Mình học được kiến thức sát với thực tế và hơn thế nữa, khi làm việc tại CBA đam mê trong mình được khơi gợi và được ‘tiếp lửa’ để duy trì sự cống hiến liên tục trong công việc.”</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- #block-our-clients -->
@endsection
