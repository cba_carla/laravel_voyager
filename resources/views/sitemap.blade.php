@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Pages</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Sitemap</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">

			<div class="col-md-4">
				<ul class="nav nav-list mb-xl">
					<li>
						<a href="/">@lang('strings.home')</a>
					</li>
					<li>
						<a href="/blog">@lang('strings.blogs')</a>
						<ul>
							<li><a href="/blog/search">@lang('strings.search')</a></li>
						</ul>
					</li>

					<li>
						<a href="/contact">@lang('strings.contacts')</a>
					</li>

					<li>
						<a href="/event">@lang('strings.events')</a>
					</li>

					<li>
						<a href="/gallery">@lang('strings.galleries')</a>
					</li>

					<li>
						<a href="/product">@lang('strings.products')</a>
						<ul>
							<li><a href="/product/search">@lang('strings.search')</a></li>
						</ul>
					</li>

					<li>
						<a href="/recruit">@lang('strings.recruits')</a>
					</li>

					<li>
						<a href="/service">@lang('strings.services')</a>
					</li>

					<li>
						<a href="/sitemap">@lang('strings.sitemap')</a>
					</li>

					<li>
						<a href="/team">@lang('strings.teams')</a>
					</li>
				</ul>
			</div>

			<div class="col-md-6 col-md-offset-2 hidden-xs">
				<h2 class="mb-lg">Who <strong>We Are</strong></h2>
				<p class="lead">Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc vehicula lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet.</p>
				<ul class="list list-icons mt-xl">
					<li><i class="fa fa-check"></i>Fusce sit amet orci quis arcu vestibulum vestibulum sed ut felis. Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
					<li><i class="fa fa-check"></i>Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
					<li><i class="fa fa-check"></i>Fusce sit amet orci quis arcu vestibulum vestibulum sed ut felis. </li>
					<li><i class="fa fa-check"></i>Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
					<li><i class="fa fa-check"></i>Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
					<li><i class="fa fa-check"></i>Fusce sit amet orci quis arcu vestibulum vestibulum sed ut felis. </li>
					<li><i class="fa fa-check"></i>Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
				</ul>
			</div>

		</div>

	</div>
@endsection
