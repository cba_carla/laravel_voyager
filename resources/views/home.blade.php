@extends('layouts.app',['pageId' => 'pageHome'])
@section('main-content')
	@include('includes.mainslider')
	<div class="home-intro" id="home-intro">
		<div class="container">

			<div class="row">
				<div class="col-md-8">
					<p>
						Các bài học giáo lý của các em được viết trong mục <em>Bài học giáo lý</em>
						<span>Các em nhớ chép bài thường xuyên nha!</span>
					</p>
				</div>
				<div class="col-md-4">
					<div class="get-started">
						<a href="#" class="btn btn-lg btn-primary">Bài học giáo lý</a>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="container">

		<div class="row center">
			<div class="col-md-12">
				<h2 class="mb-sm word-rotator-title">
					Hiện nay xứ đoàn TNTT Kitô Vua có <strong>3 phân ngành</strong> chính <br>và <strong>3 ban bộ </strong> trong Khối Phụng Vụ
				</h2>
				<p class="lead">
					Số lượng các em khoảng 500 em độ tuổi từ 7 đến 18 tuổi.
				</p>
			</div>
		</div>

	</div>

	<div class="container">
		<div class="row center">
			<div class="col-md-12">
				<div style="margin: 45px 0px -30px; overflow: hidden;">
					<img src="img/dark-and-light.jpg" class="img-responsive appear-animation" data-appear-animation="fadeInUp" alt="dark and light">
				</div>
			</div>
		</div>
	</div>

	<section class="section">
		<div class="container">

			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="col-md-4">
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-group"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Ấu Nhi</h4>
									<p class="tall">là phân ngành nhỏ tuổi nhất xứ đoàn, độ tuổi từ 7 đến 10 tuổi</p>
								</div>
							</div>
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-file"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Lễ sinh</h4>
									<p class="tall">là ban bộ phụ trách giúp lễ cho tất cả các thánh lễ trong giáo xứ</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-group"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Thiếu nhi</h4>
									<p class="tall">là phân ngành năng động, độ tuổi từ 11 đến 14 tuổi</p>
								</div>
							</div>
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-music"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Ca đoàn</h4>
									<p class="tall">Là ban bộ gồm các em thiếu nhi. Phục vụ lễ thiếu nhi lúc 15g25</p>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-group"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Nghĩa Sỹ</h4>
									<p class="tall">Là phân ngành lớn nhất xứ đoàn, độ tuổi từ 15 đến 18 tuổi</p>
								</div>
							</div>
							<div class="feature-box feature-box-style-2">
								<div class="feature-box-icon">
									<i class="fa fa-book"></i>
								</div>
								<div class="feature-box-info">
									<h4 class="mb-none">Đọc sách thánh</h4>
									<p class="tall">Là ban bộ gồm các em thiếu nhi. Phục vụ lễ thiếu nhi lúc 15g25</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container">
		<div class="row">
			<div class="counters">
				<div class="col-md-3 col-sm-6">
					<div class="counter counter-dark">
						<i class="fa fa-child"></i>
						<strong data-to="25000" data-append="+">0</strong>
						<label>Thiếu nhi</label>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="counter counter-dark">
						<i class="fa fa-star"></i>
						<strong data-to="15">0</strong>
						<label>truy cập</label>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="counter counter-dark">
						<i class="fa fa-pencil-square-o"></i>
						<strong data-to="352">0</strong>
						<label>bài viết</label>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="counter counter-dark">
						<i class="fa fa-bar-chart"></i>
						<strong data-to="178">0</strong>
						<label>High Score</label>
					</div>
				</div>
			</div>
		</div>

		@if(isset($news) && $news->count())
		<hr class="tall">
		<section class="parallax section-text-light section-parallax section-center m-none p-none" data-plugin-parallax data-plugin-options="{'speed': 0.5}">
			<div class="container">
				<div class="row">
					<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': true, 'autoplay': true}">
						@foreach($news as $item)
						<div>
							<div class="col-md-4">
								<img class="img-responsive mt-xl appear-animation" src="/storage/{{$item->image}}" alt="{{$item->title}}" data-appear-animation="fadeInLeft">
							</div>
							<div class="col-md-7 col-md-offset-1">
								<h2 class="mt-xl">{{$item->title}}</h2>
								<p class="lead">{{$item->excerpt}}</p>
								<p>{!! str_limit($item->body,50) !!}</p>
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</section>
		@endif

		@if(isset($documents) && $documents->count())
		<hr class="tall">
		<section class="parallax section-text-light section-parallax section-center m-none p-none" data-plugin-parallax data-plugin-options="{'speed': 0.5}">
			<div class="container">
				<div class="row">
					<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': true, 'autoplay': true}">
						@foreach($documents as $item)
						<div>
							<div class="col-md-7">
								<h2 class="mt-xl">{{$item->title}}</h2>
								<p class="lead">{{$item->excerpt}}</p>
								<p>{!! str_limit($item->body,50) !!}</p>
							</div>
							<div class="col-md-4 col-md-offset-1 mt-xl">
								<img class="img-responsive appear-animation" src="/storage/{{$item->image}}" alt="{{$item->title}}" data-appear-animation="fadeInRight">
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
		</section>
		@endif
		<hr class="tall">

		<div class="row center">
			<div class="col-md-12">
				<h2 class="word-rotator-title mt-xl">
					Dấu vết <strong>ký ức</strong>
				</h2>
				<!-- <p class="lead mb-xl">
					Check out what we have been doing
				</p> -->
			</div>
		</div>

	</div>

	<ul class="image-gallery sort-destination full-width mb-none">
		<li class="isotope-item">
			<div class="image-gallery-item mb-none">
				<a href="#">
					<span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
						<span class="thumb-info-wrapper">
							<img src="img/projects/project.jpg" class="img-responsive" alt="">
							<span class="thumb-info-title">
								<span class="thumb-info-inner">Project Title</span>
								<span class="thumb-info-type">Project Type</span>
							</span>
							<span class="thumb-info-action">
								<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
							</span>
						</span>
					</span>
				</a>
			</div>
		</li>
		
	</ul>

	<section class="parallax section section-text-light section-parallax section-center mt-none" data-plugin-parallax data-plugin-options="{'speed': 1.5}" data-image-src="img/parallax-2.jpg">
		<div class="container">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="owl-carousel owl-theme nav-bottom rounded-nav" data-plugin-options="{'items': 1, 'loop': false}">
						<div>
							<div class="col-md-12">
								<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
									<div class="testimonial-author">
										<img src="img/clients/client-1.jpg" class="img-responsive img-circle" alt="">
									</div>
									<blockquote>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce ante tellus, convallis non consectetur sed, pharetra nec ex.</p>
									</blockquote>
									<div class="testimonial-author">
										<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div class="col-md-12">
								<div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
									<div class="testimonial-author">
										<img src="img/clients/client-1.jpg" class="img-responsive img-circle" alt="">
									</div>
									<blockquote>
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed eget risus porta, tincidunt turpis at, interdum tortor. Suspendisse potenti. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
									</blockquote>
									<div class="testimonial-author">
										<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- <div class="container">
		<div class="row center">
			<div class="col-md-12">
				<h2 class="mb-none word-rotator-title mt-lg">
					We're not the only ones
					<strong>
						<span class="word-rotate" data-plugin-options="{'delay': 3500, 'animDelay': 400}">
							<span class="word-rotate-items">
								<span>excited</span>
								<span>happy</span>
							</span>
						</span>
					</strong>
					about Porto Template...
				</h2>
				<p class="lead">25,000+ customers in more than 100 countries use Porto Template.</p>
			</div>
		</div>
		<div class="row center mt-xl">
			<div class="owl-carousel owl-theme" data-plugin-options="{'items': 6, 'autoplay': true, 'autoplayTimeout': 3000}">
				<div><a href="#">
					<img class="img-responsive" src="img/clients/client-1.jpg" alt="">
				</a></div>
			</div>
		</div>
	</div> -->
@endsection
