@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Blog</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Blog Post</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="blog-posts single-post">

					<article class="post post-large blog-single-post">
						@if($event->photo_format->count())
						<div class="post-image">
							<div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
								@foreach($event->photo_format as $photo)
								<div>
									<div class="img-thumbnail">
										<img class="img-responsive" src="{{ $photo->url }}" alt="">
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@endif

						<div class="post-date">
							<span class="day">{{ $event->created_at->format('d') }}</span>
							<span class="month">{{ $event->created_at->format('M') }}</span>
						</div>

						<div class="post-content">

							<h2><a href="/blog/{{ $event->id }}/detail">{{ $event->title }}</a></h2>

							<div class="post-meta">
								<span><i class="fa fa-clock-o"></i> Start date {{ $event->start_date->format('d/m/Y H:i') }} </span>
								<span><i class="fa fa-clock-o"></i> End date {{ $event->start_date->format('d/m/Y H:i') }} </span>
							</div>

							{!! $event->content !!}

							<div class="post-block post-share">
								<h3 class="heading-primary"><i class="fa fa-share"></i>Share this post</h3>

								<!-- AddThis Button BEGIN -->
								<div class="addthis_toolbox addthis_default_style ">
									<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
									<a class="addthis_button_tweet"></a>
									<a class="addthis_button_pinterest_pinit"></a>
									<a class="addthis_counter addthis_pill_style"></a>
								</div>
								<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
								<!-- AddThis Button END -->

							</div>

							<div class="post-block post-author clearfix">
								<h3 class="heading-primary"><i class="fa fa-user"></i>{{ $event->organization }}</h3>
								<div class="img-thumbnail">
									<img src="/img/avatars/avatar.jpg" alt="">
								</div>
								<p><strong class="name">John Doe</strong></p>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui. </p>
							</div>

						</div>
					</article>

				</div>
			</div>

		</div>


	</div>
@endsection
