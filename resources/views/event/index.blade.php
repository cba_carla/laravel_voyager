@extends('layouts.app')
@section('main-content')
	<section class="page-header">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="/">Home</a></li>
						<li class="active">Blog</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h1>Full Width</h1>
				</div>
			</div>
		</div>
	</section>

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="blog-posts">
				@if(isset($events))	
					@foreach($events as $event)
					<article class="post post-large">
						@if($event->photo_format->count())
						<div class="post-image">
							<div class="owl-carousel owl-theme" data-plugin-options="{'items':1}">
								@foreach($event->photo_format as $photo)
								<div>
									<div class="img-thumbnail">
										<img class="img-responsive" src="{{ $photo->url }}" alt="">
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@endif

						<div class="post-date">
							<span class="day">{{ $event->start_date->format('d') }}</span>
							<span class="month">{{ $event->start_date->format('M') }}</span>
						</div>

						<div class="post-content">

							<h2><a href="/event/{{ $event->id }}/detail">{{ $event->title }}</a></h2>
							@if($event->excerpt)<p>{{ $event->excerpt }} [...]</p>@endif

							<div class="post-meta">
								<span><i class="fa fa-clock-o"></i> Start date {{ $event->start_date->format('d/m/Y H:i') }} </span>
								<span><i class="fa fa-clock-o"></i> End date {{ $event->start_date->format('d/m/Y H:i') }} </span>
								<a href="/event/{{ $event->id }}/detail" class="btn btn-xs btn-primary pull-right">Read more...</a>
							</div>

						</div>
					</article>
					@endforeach

					{{ $events->links() }}
					@endif
				</div>
			</div>

		</div>

	</div>

@endsection
