<?php
namespace App\Helpers;
use Cache;
use DB;
use Request;

class MenuHelper
{
	public static $menu = "";

	public static function AddMenu($args = null)
	{
		if(is_array($args)) {
			foreach ($args as $k => $v) {
				switch ($v['type']) {
					case 'head':
						$html = '<li class="header">'.$v['title'].'</li>';
						break;
					case 'item':
						$html = '<li class="treeview"><a href="'.$v['url'].'">';
						$html .= '<i class="fa '.$v['class_icon'].'"></i> <span>'.$v['title'].'</span>';
						$html .= '</a></li>';
						break;
					
					default:
						break;
				}
			}
		}
		self::$menu .= $html;
	}
}