<?php
namespace App\Helpers;
use Cache;
use DB;
use Request;
use Lang;

class Helper
{
	public static function CountDb($table = null)
	{
		if(empty($table)) 
			return 0;
		$minutes = 1;
		return Cache::remember($table, $minutes, function () use ($table) {
			return DB::table($table)->get()->count();
		});
	}

	public static function clearCache($table = null) {
		if(!$table){
			return null;
		}
		$value = Cache::pull($table);
	}

	public static function formatBytes($bytes, $precision = 2) { 
		$base = log($bytes, 1024);
		$suffixes = array('', 'KB', 'MB', 'GB', 'TB');
		return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
	}

	public static function getPhotos($args)
	{
		if(!empty($args)){
			$args_ids = explode(',', $args);
			foreach ($args_ids as $k => $id) {
				$item = DB::table('photos')->where('id', $id)->get()->toArray();
				if(count($item)){
					self::returnLi( $item[0] );
				} else {
					echo <<<MES
		<li class="photo_{$id} error not_in_data" data-id="{$id}"><div class="info-box bg-gray">
		  <span class="info-box-icon"><i class="fa fa fa-remove"></i></span><div class="info-box-content">
		  <span class="info-box-text fileName">&nbsp;</span><span class="info-box-number fileTitle">Photo #{$id} NOT found</span>
		  <div class="progress"><div class="progress-bar fileProgress" style="width: 100%"></div></div>
		  <a href="javascript:;" class="cancel not_in_data" data-id="{$id}">Remove</a>
		  </div></div></li>
MES;
				}
			}
		}
	} // END: returnPhoto()

	public static function returnLi($photo)
	{
		$icon = in_array($photo->extension, ['jpg','png','gif']) ? '<img src="'.$photo->url.'" width=300 height=auto class="imgUploading">' : '<i class="fa fa fa-cloud-upload"></i>';
		$size = self::formatBytes($photo->size);
		$html =<<<MES
		<li class="photo_{$photo->id} done" data-id="{$photo->id}"><div class="info-box bg-gray">
		  <span class="info-box-icon">{$icon}</span><div class="info-box-content">
		  <span class="info-box-text fileName">{$photo->name}</span><span class="info-box-number fileTitle">{$photo->name}</span>
		  <div class="progress"><div class="progress-bar fileProgress" style="width: 100%"></div></div>
		  <strong class="fileSize">{$size}</strong> | <span class="progress-description"></span>
		  <a href="javascript:;" class="cancel" data-id="{$photo->id}">Remove</a>
		  </div></div></li>
MES;
		echo $html;
	} // END: returnLi()

	public static function AdminCategoryTD($datas, $parent = 0, $depth=0){
		if($depth > 5) return ''; // Make sure not to have an endless recursion
		$tree = '';
		for($i=0, $ni=count($datas); $i < $ni; $i++){
			if($datas[$i]['parent'] == $parent){
				$tree .= '<tr>';
				$tree .= '<td>'.$datas[$i]['id'].'</td>';
				$tree .= '<td>'.str_repeat('-', $depth).$datas[$i]['name'].'</td>';
				$tree .= '<td>'.$datas[$i]['count_post'].'</td>';
				$tree .= '<td>'.date("d/m/Y",strtotime($datas[$i]['created_at'])).'</td>';
				$tree .= '<td class="text-center">
			  <div class="btn-group">
				<a href="/admin/category/?id='.$datas[$i]['id'].'" class="btn btn-default btn-xs text-green"><i class="fa fa-edit"></i> Edit</a>
				<a href="/admin/category/'.$datas[$i]['id'].'/destroy" class="btn btn-default btn-xs text-red" data-alert="'.Lang::get('messages.delete').'"><i class="fa fa-trash-o"></i> Remove</a>
			  </div>
			</td>';
				$tree .= self::AdminCategoryTD($datas, $datas[$i]['id'], $depth+1);
				$tree .= '</tr>';
			}
		}
		return $tree;
	}


	public static function getFrontendCategory($datas, $parent = 0, $depth=0){
		if($depth > 5) return ''; // Make sure not to have an endless recursion
		$tree = $depth>0?'<ul>':"";
		for($i=0, $ni=count($datas); $i < $ni; $i++){
			if($datas[$i]['parent'] == $parent){
				$tree .= '<li>';
				$count = $datas[$i]['count_post'] ? '('.$datas[$i]['count_post'].')' : "";
				$tree .= '<a href="/blog/category/'.$datas[$i]['id'].'">'.$datas[$i]['name'].' '.$count.'</a>';
				$tree .= self::getFrontendCategory($datas, $datas[$i]['id'], $depth+1);
				$tree .= '</li>';
			}
		}
		$tree .= $depth>0?'</ul>':"";
		return $tree;
	}

	// public static function generatePageTree1($datas, $parent = 0, $depth=0){
	// 	if($depth > 5) return ''; // Make sure not to have an endless recursion
	// 	$tree = '<ul>';
	// 	for($i=0, $ni=count($datas); $i < $ni; $i++){
	// 		if($datas[$i]['parent'] == $parent){
	// 			$tree .= '<li>';
	// 			$tree .= $datas[$i]['name'];
	// 			$tree .= self::generatePageTree1($datas, $datas[$i]['id'], $depth+1);
	// 			$tree .= '</li>';
	// 		}
	// 	}
	// 	$tree .= '</ul>';
	// 	return $tree;
	// }

	// public static function generatePageTree2($datas, $parent = 0, $depth = 0){
	// 	if($depth > 5) return ''; // Make sure not to have an endless recursion
	// 	$tree = '';
	// 	for($i=0, $ni=count($datas); $i < $ni; $i++){
	// 		if($datas[$i]['parent'] == $parent){
	// 			$tree .= str_repeat('-', $depth);
	// 			$tree .= $datas[$i]['name'] . '<br/>';
	// 			$tree .= self::generatePageTree2($datas, $datas[$i]['id'], $depth+1);
	// 		}
	// 	}
	// 	return $tree;
	// }

	public static function paresUrl($str = "")
	{
		parse_str($str, $parse_str);
		parse_str($_SERVER['QUERY_STRING'], $query);
		return urldecode('?'.http_build_query($parse_str+$query));
	} // END: paresUrl()
}
?>