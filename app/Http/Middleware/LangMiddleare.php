<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Request;

class LangMiddleare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dump(Session::get('locale'));
        $lang = 'vi';
        $args = ['vi','en'];
        $locale = Session::get('locale');
        $segment = $request->segment(1);
        // Get url
        $path = explode('/', $_SERVER['REQUEST_URI']);
        // Remove val empty
        $path = array_filter($path);
        // Get first (Only PHP 5.4+)
        $path = array_values($path);

        if( $request->method() != "GET" || count($path) == 0 || $locale == null){
            return $next($request);
        }

        if( in_array( $segment, $args) && $segment == $locale){
            return $next($request);
        }
        if (in_array($locale, $args)){
           $lang = $locale;
        }
        
        if(in_array($path[0], $args)){
            // Different lang -> replate
            $path[0] = $lang;
        }else{
            // Not prefix lang -> add to first
            array_unshift($path, $lang);
        }
        $path = implode('/', $path);

        return redirect($path);
    }
}
