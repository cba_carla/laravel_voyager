<?php
/**
 * Controller genrated using LaraAdmin
 * Help: http://laraadmin.com
 */

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $childs_cate_1 = Category::find(1)->children->pluck('id')->toArray();
        $news          = Post::select()
            ->whereIn('category_id', $childs_cate_1)
            ->published()
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        $childs_cate_2 = Category::find(2)->children->pluck('id')->toArray();
        $documents          = Post::select()
            ->whereIn('category_id', $childs_cate_2)
            ->published()
            ->orderBy('created_at', 'desc')
            ->take(5)
            ->get();
        
        return view('home', compact('news','documents'));
    }
}
