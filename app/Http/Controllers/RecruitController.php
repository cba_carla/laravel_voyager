<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class RecruitController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        /*$roleCount = \App\Role::count();
		if($roleCount != 0) {
			if($roleCount != 0) {
				return view('recruit');
			}
		} else {
			return view('errors.error', [
				'title' => 'Migration not completed',
				'message' => 'Please run command <code>php artisan db:seed</code> to generate required table data.',
			]);
		}*/
		return view('recruit.index');
    }
    public function detail($id)
    {
        $url = $this->url;
        $data = ($id) ? Recruit::find($id) : null;
        if($data == null){
            return abort('404');
        }
        return view('recruit::detail', compact('data', 'url'));

    }
}
