<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Contact;
use Session;

class ContactController extends Controller
{
	use ValidatesRequests, AuthorizesRequests;
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        
		return view('contact');
    }

    public function create_save(Request $request)
    {
        $this->validator($request);

        Contact::create($request->all());

        Session::flash('messages', '<strong>Success!</strong> Your message has been sent to us.');
        
        return redirect()->back();
    }

    protected function validator($request)
    {
        $this->validate($request, [
            'title' => 'required|min:3',
            'content' => 'required|min:3',
            'name' => 'required|min:3',
            'email' => 'required|email',
            'phone' => 'required|min:9|max:12|numeric|phone_number',
            'address' => 'required|min:3',
        ]);
    }
}
