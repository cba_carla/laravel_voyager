<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Helpers\MenuHelper;
use Session;
/*use Modules\Team\Entities\Team;
use Modules\Team\Entities\Member;*/
use Illuminate\Support\Facades\Input;

class TeamController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        /*$teams = Team::all();
        $members = Member::all();
        return view('team::index', compact('teams','members'));*/
        return view('team');
    }

}
