<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Album;
use App\Models\Photo;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        /*$albums = Album::where('status','publish')->get();
        return view('gallery::index',compact('albums'));*/
        return view('gallery');
    }
}
