<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use App\Models\Event;

class EventController extends Controller
{
    public function index()
    {
        //$events = Event::paginate(2);
        //return view('event::index', compact('events'));
        return view('event.index');
    }

    public function detail($id)
    {
        //$event = Event::find($id);
        /*if($event == null){
            return abort('404');
        }*/
        //return view('event::detail', compact('event'));
        return view('event.detail');
    }
}
