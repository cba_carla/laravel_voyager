<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Event extends Model
{
    protected $fillable = array('title', 'organization', 'start_date', 'end_date' , 'address','quantity','content', 'photo');
    protected $table = 'events';
    
    public function getStartDateAttribute()
	{
	    return Carbon::parse($this->attributes['start_date']); 
	}

	public function getEndDateAttribute()
	{
	    return Carbon::parse($this->attributes['end_date']); 
	}

    public function getPhotoFormatAttribute($value)
    {
        return DB::table('photos')->whereIn('id', explode(',', $this->photo))->get();
    }
}
