<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['author_id', 'category_id', 'title', 'excerpt', 'body', 'image', 'status', 'created_at'];
    protected $table    = 'posts';

    const PUBLISHED = 'PUBLISHED';

    public function category()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function getPublicAttribute()
    {
        return Carbon::parse($this->attributes['created_at']);
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'author_id');
    }

    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }
}
