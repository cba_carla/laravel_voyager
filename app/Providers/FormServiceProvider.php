<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('eText', 'components.form.text', ['label', 'name', 'data' => null, 'attributes' => []]);
        Form::component('ePassword', 'components.form.password', ['label', 'name', 'data' => null, 'attributes' => []]);
        Form::component('eTextarea', 'components.form.textarea', ['label', 'name', 'data' => null, 'attributes' => []]);
        Form::component('eSelect', 'components.form.select', ['label', 'name', 'data' => null, 'args'=>[], 'attributes'=>[]]);
        Form::component('eSelectmulti', 'components.form.selectmulti', ['label', 'name', 'data' => null, 'args'=>[], 'attributes'=>[]]);
        Form::component('eUpload', 'components.form.upload', ['label', 'name', 'data' => null, 'attributes'=>null]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
